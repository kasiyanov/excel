from django.shortcuts import render
from django.http import HttpResponse
from openpyxl import load_workbook

from main.models import Branch, Employees, Region


def index(request):
    dates = Branch.objects.all()
    if len(dates) == 0:
        book = load_workbook('branch.xlsx')
        sheet = book.active
        rows = sheet.rows
        headers = [cell.value for cell in next(rows)]
        for row in rows:
            data = []
            for title, cell in zip(headers, row):
                data.append(cell.value)
            region = Region.objects.filter(number=data[2]).first()
            excel = Branch(number=data[0], name=data[1], region=region)
            excel.save()

    return render(request, 'main/index.html')


def second(request):
    datess = Employees.objects.all()
    if len(datess) == 0:
        book2 = load_workbook('emplists.xlsx')
        sheet2 = book2.active
        rows = sheet2.rows
        headers = [cell.value for cell in next(rows)]
        for row in rows:
            data = []
            for title, cell in zip(headers, row):
                data.append(cell.value)
            excel = Employees(number=data[0], name=data[1], employee=data[2],
                              employeeBranch=Branch.objects.filter(number=data[0]).first())
            excel.save()

    return render(request, 'main/second.html')



