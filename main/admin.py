from django.contrib import admin
from main.models import Branch, Employees, Region


class BranchAdmin(admin.ModelAdmin):
    list_display = ['name', 'number']


admin.site.register(Branch, BranchAdmin)


class EmployeesAdmin(admin.ModelAdmin):
    list_display = ['name', 'number', 'employee']


admin.site.register(Employees, EmployeesAdmin)


class RegionAdmin(admin.ModelAdmin):
    list_display = ['name', 'number']


admin.site.register(Region, RegionAdmin)
