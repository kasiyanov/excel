# Generated by Django 3.2.6 on 2021-09-07 05:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_alter_branch_regionnumber'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='branch',
            name='regionNumber',
        ),
    ]
