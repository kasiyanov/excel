from django.db import models


class Region(models.Model):
    number = models.CharField(max_length=200)
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Region'
        verbose_name_plural = 'Регион'

    def __str__(self):
        return self.name


class Branch(models.Model):
    number = models.CharField(max_length=200)
    name = models.CharField(max_length=255)
    region = models.ForeignKey(Region, verbose_name='region', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = 'Branch'
        verbose_name_plural = 'Филиалы'

    def __str__(self):
        return self.number


class Employees(models.Model):
    number = models.CharField(max_length=200)
    name = models.CharField(max_length=255)
    employee = models.CharField(max_length=255)
    employeeBranch = models.ForeignKey(Branch, verbose_name='branch', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = 'Branch'
        verbose_name_plural = 'Сотрудники'

    def __str__(self):
        return self.name


